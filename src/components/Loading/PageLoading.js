import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles(theme => ({
  container: {
    position: "fixed",
    top: 0,
    left: 0,
    backgroundColor: "#FFFFFF",
    height: "100%",
    minHeight: "100vh",
    width: "100%",
    minWidth: "100vw",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  progress: {
    margin: theme.spacing(2)
  }
}));

export default function PageLoaing() {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <CircularProgress className={classes.progress} />
    </div>
  );
}
