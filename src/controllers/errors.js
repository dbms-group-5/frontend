export const error_codes = {
  success: 10,
  failed: 11,
  not_found: 12,
  missing_field: 13
};
