import makeRequest, { makeFormRequest } from "./requests.js";

const create = (title, description, author_id, content) => {
  let body = new FormData();
  body.append("title", title);
  body.append("description", description);
  body.append("author_id", author_id);
  body.append("content", content);
  return makeFormRequest("/posts/", {
    method: "POST",
    body: body
  });
};

const get_content = id => {
  return makeRequest("/posts/content/" + id, {
    method: "GET"
  });
};

const update = (id, title, description, author_id, content) => {
  let body = new FormData();
  body.append("id", id);
  body.append("title", title);
  body.append("description", description);
  body.append("author_id", author_id);
  body.append("content", content);
  return makeFormRequest("/posts/" + id, {
    method: "PUT",
    body: body
  });
};

const get_all = () => {
  return makeRequest("/posts/", {
    method: "GET"
  });
};

const delete_post = id => {
  return makeRequest("/posts/" + id, {
    method: "DELETE"
  });
};

export { create, get_all, update, delete_post, get_content };
