const makeRequest = (url, options) => {
  const newUrl = process.env.REACT_APP_API_URL + url;
  options.headers = {
    "Content-Type": "application/json",
    Accept: "application/json"
  };
  options.credentials = "include";
  return fetch(newUrl, options)
    .then(response => {
      // DELETE and 204 do not return data by default
      // Using json will report error
      if (/*options.method === "DELETE" || */ response.status === 204) {
        response = response.text();
      } else if (options.method === "HEAD") {
        response = response.status;
      } else if (response.headers.get("Content-Type") === "application/json") {
        response = response.json();
      } else if (
        response.headers.get("Content-Type") === "text/md; charset=utf-8"
      ) {
        response = response.text();
      } else {
        response.blob().then(blob => {
          response = URL.createObjectURL(blob);
        });
      }
      // response.then(res => {
      //   console.log(res);
      // });
      return response;
    })
    .catch(e => {
      console.log(e);
    });
};

const makeFormRequest = (url, options) => {
  const newUrl = process.env.REACT_APP_API_URL + url;
  options.credentials = "include";
  return fetch(newUrl, options)
    .then(response => {
      // DELETE and 204 do not return data by default
      // Using json will report error
      if (/*options.method === "DELETE" || */ response.status === 204) {
        response = response.text();
      } else if (options.method === "HEAD") {
        response = response.status;
      } else if (response.headers.get("Content-Type") === "application/json") {
        response = response.json();
      } else if (
        response.headers.get("Content-Type") === "text/md; charset=utf-8"
      ) {
        response = response.text();
      } else {
        response.blob().then(blob => {
          response = URL.createObjectURL(blob);
        });
      }
      // response.then(res => {
      //   console.log(res);
      // });
      return response;
    })
    .catch(e => {
      console.log(e);
    });
};

export default makeRequest;
export { makeFormRequest };
