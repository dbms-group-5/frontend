import makeRequest from "./requests.js";

const create = name => {
  let body = {
    name: name
  };
  return makeRequest("/roles/", {
    method: "POST",
    body: JSON.stringify(body)
  });
};

const update = (id, name) => {
  let body = {
    id: id,
    name: name
  };
  return makeRequest("/roles/" + id, {
    method: "PUT",
    body: JSON.stringify(body)
  });
};

const get_all = () => {
  return makeRequest("/roles/", {
    method: "GET"
  });
};

const delete_role = id => {
  return makeRequest("/roles/" + id, {
    method: "DELETE"
  });
};

export { create, get_all, update, delete_role };
