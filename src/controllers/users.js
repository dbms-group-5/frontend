import makeRequest from "./requests.js";

const is_au_yet = () => {
  return makeRequest("/users/is-authenticated", {
    method: "GET"
  });
};

const login = (usernameemail, password) => {
  let body;
  if (usernameemail.indexOf("@") > -1) {
    body = {
      email: usernameemail,
      password: password
    };
  } else {
    body = {
      username: usernameemail,
      password: password
    };
  }
  return makeRequest("/users/login", {
    method: "POST",
    body: JSON.stringify(body)
  });
};

const logout = () => {
  return makeRequest("/users/logout", {
    method: "GET"
  });
};

const register = (
  username,
  password,
  name,
  birthday,
  phone,
  email,
  gender,
  role_id
) => {
  let body = {
    username: username,
    password: password,
    name: name,
    birthday: birthday,
    phone: phone,
    email: email,
    gender: gender,
    role_id: role_id
  };
  return makeRequest("/users/register", {
    method: "POST",
    body: JSON.stringify(body)
  });
};

const update = (
  id,
  username,
  password,
  name,
  birthday,
  phone,
  email,
  gender,
  role_id
) => {
  let body = {
    id: id,
    username: username,
    password: password,
    name: name,
    birthday: birthday,
    phone: phone,
    email: email,
    gender: gender,
    role_id: role_id
  };
  return makeRequest("/users/" + id, {
    method: "PUT",
    body: JSON.stringify(body)
  });
};

const get_all = () => {
  return makeRequest("/users/", {
    method: "GET"
  });
};

const delete_user = id => {
  return makeRequest("/users/" + id, {
    method: "DELETE"
  });
};

const change_password = (user_id, old_pass, new_pass) => {
  let body = JSON.stringify({
    old_password: old_pass,
    new_password: new_pass
  });
  return makeRequest("/users/change-password/" + user_id, {
    method: "PUT",
    body: body
  });
};

export {
  login,
  logout,
  register,
  change_password,
  is_au_yet,
  get_all,
  update,
  delete_user
};
