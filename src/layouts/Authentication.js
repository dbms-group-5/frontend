import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import { login_routes } from "routes.js";

import { makeStyles } from "@material-ui/core/styles";

import CoverImage from "assets/img/cover-2.jpg";

const switchRoutes = (
  <Switch>
    {login_routes.map((prop, key) => {
      if (prop.layout === "/authentication") {
        return (
          <Route
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
          />
        );
      }
      return null;
    })}
    <Redirect from="/authentication" to="/authentication/login" />
  </Switch>
);

const styles = {
  container: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundImage: `url(${CoverImage})`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    width: "100%",
    height: "100%",
    margin: "0px",
    padding: "0px",
    minHeight: "100vh"
  }
};

const useStyles = makeStyles(styles);

export default function Authentication() {
  const classes = useStyles();
  return <div className={classes.container}>{switchRoutes}</div>;
}
