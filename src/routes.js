/*!

=========================================================
* Material Dashboard React - v1.8.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import LibraryBooks from "@material-ui/icons/LibraryBooks";
import BubbleChart from "@material-ui/icons/BubbleChart";
import PermMedia from "@material-ui/icons/PermMedia";
import Accessibility from "@material-ui/icons/Accessibility";
import LocationOn from "@material-ui/icons/LocationOn";
import Notifications from "@material-ui/icons/Notifications";
import LocationCity from "@material-ui/icons/LocationCity";
// core components/views for Admin layout
import DashboardPage from "views/Dashboard/Dashboard.js";
import UserProfile from "views/UserProfile/UserProfile.js";
import TableList from "views/TableList/TableList.js";
import Typography from "views/Typography/Typography.js";
import Icons from "views/Icons/Icons.js";
import Maps from "views/Maps/Maps.js";
import NotificationsPage from "views/Notifications/Notifications.js";
import Places from "views/Places/Places.js";
import Users from "views/Users/Users.js";
import Posts from "views/Posts/Posts.js";
import Images from "views/Images/Images.js";
import Roles from "views/Roles/Roles.js";
// core components for Empty Layout
import Login from "views/Authentication/Login.js";

const rRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/admin"
  },
  // {
  //   path: "/user",
  //   name: "User Profile",
  //   icon: Person,
  //   component: UserProfile,
  //   layout: "/admin"
  // },
  // {
  //   path: "/table",
  //   name: "Table List",
  //   icon: "content_paste",
  //   component: TableList,
  //   layout: "/admin"
  // },
  // {
  //   path: "/typography",
  //   name: "Typography",
  //   icon: LibraryBooks,
  //   component: Typography,
  //   layout: "/admin"
  // },
  {
    path: "/notifications",
    name: "Notifications",
    icon: Notifications,
    component: NotificationsPage,
    layout: "/admin"
  },
  // {
  //   path: "/places",
  //   name: "Places",
  //   icon: LocationCity,
  //   component: Places,
  //   layout: "/admin"
  // },
  {
    path: "/users",
    name: "Users",
    icon: Person,
    component: Users,
    layout: "/admin"
  },
  {
    path: "/posts",
    name: "Posts",
    icon: LibraryBooks,
    component: Posts,
    layout: "/admin"
  },
  {
    path: "/images",
    name: "Images",
    icon: PermMedia,
    component: Images,
    layout: "/admin"
  },
  {
    path: "/roles",
    name: "Roles",
    icon: Accessibility,
    component: Roles,
    layout: "/admin"
  }
];

const login_routes = [
  {
    path: "/login",
    name: "Login",
    icon: null,
    component: Login,
    layout: "/authentication"
  }
];

export default rRoutes;

export { login_routes };
