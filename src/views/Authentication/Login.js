import React from "react";
import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core/styles";
import { Typography, TextField, Button } from "@material-ui/core";

import { login } from "controllers/users.js";
import { error_codes } from "controllers/errors.js";

import hltSquareImage from "assets/img/reactlogo.png";

const useStyles = makeStyles(theme => ({
  container: {
    position: "relative",
    left: "150px",
    display: "flex",
    flexDirection: "row",
    justifyContent: "stretch",
    alignItems: "stretch",
    borderRadius: "3px",
    backgroundColor: "rgba(255,255,255,0.7)",
    width: "400px",
    margin: "0px",
    padding: "20px",
    paddingTop: "10px",
    paddingBottom: "10px"
  },
  subcontainer: {
    display: "flex",
    flex: 1,
    flexDirection: "column",
    justifyContent: "stretch",
    alignItems: "stretch",
    margin: "10px"
  },
  img: {
    display: "flex",
    justifyContent: "stretch",
    alignItems: "stretch",
    height: "170px",
    width: "170px"
  },
  button: {
    margin: theme.spacing(1),
    marginLeft: 0,
    marginRight: 0,
    background: "linear-gradient(45deg, #2196F3 30%, #21CBF3 90%)",
    fontSize: "10pt",
    boxShadow: "0 3px 5px 2px rgba(33, 150, 243, 0.2)"
  },
  caption: {
    textAlign: "center"
  },
  // Extra small devices (portrait phones, less than 576px)
  "@media screen and (max-width: 767.98px)": {
    container: {
      flexDirection: "column",
      left: 0,
      width: "100%",
      justifyContent: "center",
      alignItems: "center",
      borderRadius: 0
    }
  },
  // Medium devices (tablets, 768px and up)
  "@media screen and (min-width: 768px) and (max-width: 991.98px)": {},
  // Large devices (desktops, 992px and up)
  "@media screen and (min-width: 992px) and (max-width: 1199.98px)": {},
  // Extra large devices (large desktops, 1200px and up)
  "@media screen and (min-width: 1200px)": {}
}));

export default function Login(props) {
  const [usernameemail, setUsernameemail] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [message, setMessage] = React.useState("");
  const classes = useStyles();
  const authenticate = () => {
    setMessage("");
    login(usernameemail, password).then(res => {
      try {
        if (res.error_code === error_codes.success) {
          props.history.push("/admin");
        } else {
          setMessage("Wrong username or password");
        }
      } catch (err) {
        setMessage("Something wrong happened");
      }
    });
  };
  const loginEnter = e => {
    if (e.key === "Enter") {
      authenticate();
    }
  };
  return (
    <div className={classes.container}>
      <img src={hltSquareImage} alt="" className={classes.img} />
      <div className={classes.subcontainer}>
        <TextField
          label="Email/Username"
          fullWidth
          margin="dense"
          variant="outlined"
          type="text"
          value={usernameemail}
          onChange={e => {
            setUsernameemail(e.target.value);
          }}
          onKeyPress={e => {
            loginEnter(e);
          }}
        />
        <TextField
          label="Password"
          fullWidth
          margin="dense"
          variant="outlined"
          type="password"
          value={password}
          onChange={e => {
            setPassword(e.target.value);
          }}
          onKeyPress={e => {
            loginEnter(e);
          }}
        />
        <Button
          color="primary"
          variant="contained"
          className={classes.button}
          size="small"
          onClick={() => {
            authenticate();
          }}
        >
          Login
        </Button>
        <Typography variant="caption" color="error" className={classes.caption}>
          {message}
        </Typography>
      </div>
    </div>
  );
}

Login.propTypes = {
  history: PropTypes.object
};
