import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import Table from "components/Table/Table.js";
import TextField from "@material-ui/core/TextField";
import Icon from "@material-ui/core/Icon";
import CardIcon from "components/Card/CardIcon.js";

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "500",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const useStyles = makeStyles(styles);

export default function Images() {
  const classes = useStyles();
  const [id, setId] = React.useState("");
  const [image, setImage] = React.useState("");
  const [created, setCreated] = React.useState("");
  const [user_id, setUser_id] = React.useState("");
  const [post_id, setPost_id] = React.useState("");
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={9}>
        <Card>
          <CardHeader color="warning">
            <h4 className={classes.cardTitleWhite}>Images</h4>
            <p className={classes.cardCategoryWhite}>
              This table contains all the images in database
            </p>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="warning"
              tableHead={["ID", "Image", "User Id", "Post Id", "Created"]}
              tableData={[]}
            />
          </CardBody>
        </Card>
      </GridItem>
      <GridItem xs={12} sm={12} md={3}>
        <GridItem xs={12} sm={12} md={12}>
          <Card profile>
            <CardHeader color="warning">
              <h4 className={classes.cardTitleWhite}>Insert/Update</h4>
            </CardHeader>
            <CardBody profile>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="Id (empty for insert)"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={id}
                    onChange={e => {
                      setId(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="Image"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={image}
                    onChange={e => {
                      setImage(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="User Id"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={user_id}
                    onChange={e => {
                      setUser_id(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="Post Id"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={post_id}
                    onChange={e => {
                      setPost_id(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <Button
                    color="danger"
                    fullWidth
                    onClick={() => {
                      setImage("");
                      setUser_id("");
                      setPost_id("");
                    }}
                  >
                    Clear
                  </Button>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <Button color="warning" fullWidth>
                    Submit
                  </Button>
                </GridItem>
              </GridContainer>
            </CardBody>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={12} md={12}>
          <Card profile>
            <CardHeader color="warning">
              <h4 className={classes.cardTitleWhite}>Delete</h4>
            </CardHeader>
            <CardBody profile>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="Id"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={id}
                    onChange={e => {
                      setId(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <Button color="danger" fullWidth>
                    Submit
                  </Button>
                </GridItem>
              </GridContainer>
            </CardBody>
          </Card>
        </GridItem>
      </GridItem>
    </GridContainer>
  );
}
