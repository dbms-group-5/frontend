import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import Table from "components/Table/Table.js";
import TextField from "@material-ui/core/TextField";
import Icon from "@material-ui/core/Icon";
import CardIcon from "components/Card/CardIcon.js";

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "500",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const useStyles = makeStyles(styles);

export default function Places() {
  const classes = useStyles();
  const [name, setName] = React.useState("");
  const [code, setCode] = React.useState("");
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={9}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>
              Places - Cities - Airports
            </h4>
            <p className={classes.cardCategoryWhite}>
              This table contains all the places (cities/airports) in database
            </p>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="primary"
              tableHead={["ID", "Name", "Code", "Created"]}
              tableData={[]}
            />
          </CardBody>
        </Card>
      </GridItem>
      <GridItem xs={12} sm={12} md={3}>
        <GridItem xs={12} sm={12} md={12}>
          <Card profile>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>Insert</h4>
            </CardHeader>
            <CardBody profile>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="Name"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={name}
                    onChange={e => {
                      setName(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="Code"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={code}
                    onChange={e => {
                      setCode(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <Button
                    color="grey"
                    fullWidth
                    onClick={() => {
                      setName("");
                      setCode("");
                    }}
                  >
                    Clear
                  </Button>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <Button color="rose" fullWidth>
                    Submit
                  </Button>
                </GridItem>
              </GridContainer>
            </CardBody>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={12} md={12}>
          <Card profile>
            <CardHeader color="info">
              <h4 className={classes.cardTitleWhite}>Delete</h4>
            </CardHeader>
            <CardBody profile>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="Code"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={code}
                    onChange={e => {
                      setCode(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <Button color="rose" fullWidth>
                    Submit
                  </Button>
                </GridItem>
              </GridContainer>
            </CardBody>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={12} md={12}>
          <Card profile>
            <CardHeader color="info">
              <h4 className={classes.cardTitleWhite}>Update</h4>
            </CardHeader>
            <CardBody profile>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="Code"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={code}
                    onChange={e => {
                      setCode(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <Button color="rose" fullWidth>
                    Submit
                  </Button>
                </GridItem>
              </GridContainer>
            </CardBody>
          </Card>
        </GridItem>
      </GridItem>
    </GridContainer>
  );
}
