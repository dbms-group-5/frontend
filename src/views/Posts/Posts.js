import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import Table from "components/Table/Table.js";
import TextField from "@material-ui/core/TextField";
import Icon from "@material-ui/core/Icon";
import CardIcon from "components/Card/CardIcon.js";
import {
  create,
  delete_post,
  update,
  get_all,
  get_content
} from "controllers/posts.js";
import { error_codes } from "controllers/errors.js";
import SimpleModel from "components/Modal/Modal.js";
import SimpleModal from "components/Modal/Modal";

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "500",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const useStyles = makeStyles(styles);

export default function Posts() {
  const classes = useStyles();
  const [id, setId] = React.useState("");
  const [title, setTitle] = React.useState("");
  const [description, setDescription] = React.useState("");
  const [author_id, setAuthor_id] = React.useState("");
  const [content, setContent] = React.useState(null);
  const [data, setData] = React.useState([]);
  const [open, setOpen] = React.useState(false);
  const [tempTitle, setTempTitle] = React.useState("");
  const [tempContent, setTempContent] = React.useState("");
  const clear = () => {
    setAuthor_id("");
    setTitle("");
    setDescription("");
    setId("");
    setContent(null);
    setTempContent("");
    setTempTitle("");
  };
  const get_data = () => {
    get_all().then(res => {
      if (res.error_code == error_codes.success) {
        var result = [];
        res.data.map(x => {
          var temp = [];
          temp.push(x["id"]);
          temp.push(x["title"]);
          temp.push(x["description"]);
          temp.push(
            <Button
              color="white"
              onClick={() => {
                setTempTitle(x["title"]);
                get_content(x["id"]).then(res => {
                  setTempContent(res);
                });
                setOpen(true);
              }}
            >
              Show content
            </Button>
          );
          temp.push(x["author_id"]);
          temp.push(x["created"]);
          result.push(temp);
        });
        setData(result);
      } else {
        setData([]);
      }
    });
  };
  React.useEffect(get_data, []);
  return (
    <GridContainer>
      <SimpleModal
        open={open}
        handleClose={() => {
          setOpen(false);
        }}
        title={tempTitle}
        content={tempContent}
      />
      <GridItem xs={12} sm={12} md={9}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>Posts</h4>
            <p className={classes.cardCategoryWhite}>
              This table contains all the posts in database
            </p>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="primary"
              tableHead={[
                "ID",
                "Title",
                "Description",
                "Content",
                "Author Id",
                "Created"
              ]}
              tableData={data}
            />
          </CardBody>
        </Card>
      </GridItem>
      <GridItem xs={12} sm={12} md={3}>
        <GridItem xs={12} sm={12} md={12}>
          <Card profile>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>Insert/Update</h4>
            </CardHeader>
            <CardBody profile>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="Id (empty for insert)"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={id}
                    onChange={e => {
                      setId(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="Title"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={title}
                    onChange={e => {
                      setTitle(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="Description"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={description}
                    onChange={e => {
                      setDescription(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <input
                    accept="*.md"
                    type="file"
                    onChange={e => {
                      setContent(e.target.files[0]);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="Author Id"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={author_id}
                    onChange={e => {
                      setAuthor_id(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <Button
                    color="danger"
                    fullWidth
                    onClick={() => {
                      clear();
                    }}
                  >
                    Clear
                  </Button>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <Button
                    color="primary"
                    fullWidth
                    onClick={() => {
                      if (id === "") {
                        create(title, description, author_id, content).then(
                          res => {
                            if (
                              res &&
                              "error_code" in res &&
                              res.error_code == error_codes.failed
                            ) {
                              alert(res.message);
                            } else {
                              get_data();
                              clear();
                            }
                          }
                        );
                      } else {
                        update(id, title, description, author_id, content).then(
                          res => {
                            if (
                              res &&
                              "error_code" in res &&
                              res.error_code == error_codes.failed
                            ) {
                              alert(res.message);
                            } else {
                              get_data();
                              clear();
                            }
                          }
                        );
                      }
                    }}
                  >
                    Submit
                  </Button>
                </GridItem>
              </GridContainer>
            </CardBody>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={12} md={12}>
          <Card profile>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>Delete</h4>
            </CardHeader>
            <CardBody profile>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="Id"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={id}
                    onChange={e => {
                      setId(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <Button
                    color="danger"
                    fullWidth
                    onClick={() => {
                      delete_post(id).then(res => {
                        if (
                          res &&
                          "error_code" in res &&
                          res.error_code == error_codes.failed
                        ) {
                          alert(res.message);
                        } else {
                          get_data();
                          clear();
                        }
                      });
                    }}
                  >
                    Submit
                  </Button>
                </GridItem>
              </GridContainer>
            </CardBody>
          </Card>
        </GridItem>
      </GridItem>
    </GridContainer>
  );
}
