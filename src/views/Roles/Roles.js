import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import Table from "components/Table/Table.js";
import TextField from "@material-ui/core/TextField";
import Icon from "@material-ui/core/Icon";
import CardIcon from "components/Card/CardIcon.js";
import { create, update, get_all, delete_role } from "controllers/roles.js";
import { error_codes } from "controllers/errors.js";

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "500",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const useStyles = makeStyles(styles);

export default function Roles() {
  const classes = useStyles();
  const [name, setName] = React.useState("");
  const [id, setId] = React.useState("");
  const [data, setData] = React.useState([]);
  const get_data = () => {
    get_all().then(res => {
      if (res.error_code == error_codes.success) {
        var result = [];
        res.results.map(x => {
          var temp = [];
          temp.push(x["id"]);
          temp.push(x["name"]);
          result.push(temp);
        });
        setData(result);
      } else {
        setData([]);
      }
    });
  };
  React.useEffect(get_data, []);
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={9}>
        <Card>
          <CardHeader color="success">
            <h4 className={classes.cardTitleWhite}>Roles</h4>
            <p className={classes.cardCategoryWhite}>
              This table contains all the roles in database
            </p>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="success"
              tableHead={["ID", "Name"]}
              tableData={data}
            />
          </CardBody>
        </Card>
      </GridItem>
      <GridItem xs={12} sm={12} md={3}>
        <GridItem xs={12} sm={12} md={12}>
          <Card profile>
            <CardHeader color="success">
              <h4 className={classes.cardTitleWhite}>Insert/Update</h4>
            </CardHeader>
            <CardBody profile>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="Id (empty for insert)"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={id}
                    onChange={e => {
                      setId(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="Name"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={name}
                    onChange={e => {
                      setName(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <Button
                    color="danger"
                    fullWidth
                    onClick={() => {
                      setName("");
                      setId("");
                    }}
                  >
                    Clear
                  </Button>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <Button
                    color="success"
                    fullWidth
                    onClick={() => {
                      if (id === "") {
                        create(name).then(res => {
                          if (
                            res &&
                            "error_code" in res &&
                            res.error_code == error_codes.failed
                          ) {
                            alert(res.message);
                          } else {
                            get_data();
                          }
                        });
                      } else {
                        update(id, name).then(res => {
                          if (
                            res &&
                            "error_code" in res &&
                            res.error_code == error_codes.failed
                          ) {
                            alert(res.message);
                          } else {
                            get_data();
                          }
                        });
                      }
                    }}
                  >
                    Submit
                  </Button>
                </GridItem>
              </GridContainer>
            </CardBody>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={12} md={12}>
          <Card profile>
            <CardHeader color="success">
              <h4 className={classes.cardTitleWhite}>Delete</h4>
            </CardHeader>
            <CardBody profile>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="id"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={id}
                    onChange={e => {
                      setId(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <Button
                    color="danger"
                    fullWidth
                    onClick={() => {
                      delete_role(id).then(res => {
                        if (
                          res &&
                          "error_code" in res &&
                          res.error_code == error_codes.failed
                        ) {
                          alert(res.message);
                        } else {
                          get_data();
                        }
                      });
                    }}
                  >
                    Submit
                  </Button>
                </GridItem>
              </GridContainer>
            </CardBody>
          </Card>
        </GridItem>
      </GridItem>
    </GridContainer>
  );
}
