import React, { useEffect } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import Table from "components/Table/Table.js";
import TextField from "@material-ui/core/TextField";
import Icon from "@material-ui/core/Icon";
import CardIcon from "components/Card/CardIcon.js";
import { register, get_all } from "controllers/users.js";
import { error_codes } from "controllers/errors.js";
import { delete_user } from "controllers/users";
import { update } from "controllers/users";

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "500",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const useStyles = makeStyles(styles);

export default function Users() {
  const classes = useStyles();
  const [name, setName] = React.useState("");
  const [username, setUsername] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [birthday, setBirthday] = React.useState("");
  const [phone, setPhone] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [gender, setGender] = React.useState("");
  const [role_id, setRole_id] = React.useState("");
  const [id, setId] = React.useState("");
  const [data, setData] = React.useState([]);
  const get_data = () => {
    get_all().then(res => {
      if (res.error_code == error_codes.success) {
        var result = [];
        res.data.map(x => {
          var temp = [];
          temp.push(x["id"]);
          temp.push(x["name"]);
          temp.push(x["username"]);
          temp.push(x["birthday"]);
          temp.push(x["phone"]);
          temp.push(x["email"]);
          temp.push(x["gender"]);
          temp.push(x["role_id"]);
          temp.push(x["created"]);
          result.push(temp);
        });
        setData(result);
      } else {
        setData([]);
      }
    });
  };
  useEffect(get_data, []);
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={9}>
        <Card>
          <CardHeader color="info">
            <h4 className={classes.cardTitleWhite}>Users</h4>
            <p className={classes.cardCategoryWhite}>
              This table contains all the users in database
            </p>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="info"
              tableHead={[
                "ID",
                "Name",
                "Username",
                "Birthday",
                "Phone",
                "Email",
                "Gender",
                "Role id",
                "Created"
              ]}
              tableData={data}
            />
          </CardBody>
        </Card>
      </GridItem>
      <GridItem xs={12} sm={12} md={3}>
        <GridItem xs={12} sm={12} md={12}>
          <Card profile>
            <CardHeader color="info">
              <h4 className={classes.cardTitleWhite}>Insert/Update</h4>
            </CardHeader>
            <CardBody profile>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="Id (empty on insert)"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={id}
                    onChange={e => {
                      setId(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="Name"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={name}
                    onChange={e => {
                      setName(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="Username"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={username}
                    onChange={e => {
                      setUsername(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="Password"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={password}
                    type="password"
                    onChange={e => {
                      setPassword(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="Birthday"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={birthday}
                    onChange={e => {
                      setBirthday(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="Phone"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={phone}
                    onChange={e => {
                      setPhone(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="Email"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={email}
                    onChange={e => {
                      setEmail(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="Gender"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={gender}
                    onChange={e => {
                      setGender(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="Role Id"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={role_id}
                    onChange={e => {
                      setRole_id(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <Button
                    color="danger"
                    fullWidth
                    onClick={() => {
                      setName("");
                      setUsername("");
                      setPassword("");
                      setBirthday("");
                      setPhone("");
                      setEmail("");
                      setGender("");
                      setRole_id("");
                    }}
                  >
                    Clear
                  </Button>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <Button
                    color="info"
                    fullWidth
                    onClick={() => {
                      if (id === "") {
                        register(
                          username,
                          password,
                          name,
                          birthday,
                          phone,
                          email,
                          gender,
                          role_id
                        ).then(res => {
                          if (
                            res &&
                            "error_code" in res &&
                            res.error_code == error_codes.failed
                          ) {
                            alert(res.message);
                          } else {
                            get_data();
                          }
                        });
                      } else {
                        update(
                          id,
                          username,
                          password,
                          name,
                          birthday,
                          phone,
                          email,
                          gender,
                          role_id
                        ).then(res => {
                          if (
                            res &&
                            "error_code" in res &&
                            res.error_code == error_codes.failed
                          ) {
                            alert(res.message);
                          } else {
                            get_data();
                          }
                        });
                      }
                    }}
                  >
                    Submit
                  </Button>
                </GridItem>
              </GridContainer>
            </CardBody>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={12} md={12}>
          <Card profile>
            <CardHeader color="info">
              <h4 className={classes.cardTitleWhite}>Delete</h4>
            </CardHeader>
            <CardBody profile>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <TextField
                    label="Id"
                    fullWidth
                    variant="outlined"
                    margin="dense"
                    value={id}
                    onChange={e => {
                      setId(e.target.value);
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <Button
                    color="danger"
                    fullWidth
                    onClick={() => {
                      delete_user(id).then(res => {
                        if (res && "error_code" in res) {
                          if (res.error_code == error_codes.failed) {
                            alert(res.message);
                          } else {
                            alert("Something went wrong");
                          }
                        } else {
                          get_data();
                        }
                      });
                    }}
                  >
                    Submit
                  </Button>
                </GridItem>
              </GridContainer>
            </CardBody>
          </Card>
        </GridItem>
      </GridItem>
    </GridContainer>
  );
}
